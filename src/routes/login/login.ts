import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { UsersModel } from '../../models/booking/users';
import { ProfilesModel } from '../../models/booking/profiles';
import { RolesModel } from '../../models/booking/roles';
import { HospitalsModel } from '../../models/booking/hospitals';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const usersModel = new UsersModel();
  const profilesModel = new ProfilesModel();
  const rolesModel = new RolesModel();
  const hospitalsModel = new HospitalsModel();

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = req.body 
    // console.log('data : ',data);
    

    try {
        let user: any;
        let profile: any;
        let role: any;
        let hospital: any;

        let users :any = await usersModel.doLogin(db,data.username,data.password);
        console.log("users" ,users[0] );
        
        if(users.length > 0){
            user = await users[0];
            console.log(" : ",user);
            
            let profiles :any = await profilesModel.getByID(db,user.user_id);
            if(profiles.length > 0){

                profile = await profiles[0];
                console.log("profile : " , profile);
                
                let hospitals :any = await hospitalsModel.getByID(db,profile.hospital_id);
                
                if(hospitals.length > 0){
                    hospital=await hospitals[0];
                    console.log("hospital : " , hospital);

                }
            }
            let roles :any = await rolesModel.getByID(db,user.role_id);
            
            if(roles.length > 0){
                role = await roles[0];
                console.log("roles : ",roles[0]);
                delete role.is_active;
            }
            const date = new Date();
            const iat = Math.floor(date.getTime() / 1000);
            const exp = Math.floor((date.setDate(date.getDate() + 7)) / 1000);

            const info: any = {
              id: user.username,
              user_id: user.user_id,
              name: profile.fullname || null,
              hospital_id: hospital.hospital_id || null,
              service_id: profile.service_id || null,
              hospcode: hospital.hospital_code || null,
              hospname: hospital.hospital_name || null,
              role: role,
              email: 'ubondev@gmail.com',
              avatar: 'assets/images/avatars/brian-hughes.jpg',
              status: 'online'
            }

            console.log("info : ",info);
            
            const payload: any = {
            iat: iat,
            iss: 'UbonDev',
            exp: exp
            }
            const token = fastify.jwt.sign(payload)

        return reply.status(StatusCodes.OK)
            .send({
            status:StatusCodes.OK,
            ok: true,
            results : {accessToken: token,info: info ,tokenType: 'bearer'}
            });
        } else {
          console.log('Not Authorized Success');
          
            return reply.status(StatusCodes.OK)
            .send({
            status:204,
            ok: false,
            results : `Not Authorized Success`
            });
        }
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}