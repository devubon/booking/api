import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { CustomersModel } from '../../models/booking/customers';
import { ReservesModel } from '../../models/booking/reserves';
import { ServicesModel } from '../../models/booking/services';
import { HospitalsModel } from '../../models/booking/hospitals';
import { PeriodsModel } from '../../models/booking/periods';
import { UsersModel } from '../../models/booking/users';
import { ServiceTypesModel } from '../../models/booking/service_types';
import { SlotsModel } from '../../models/booking/slots';
import { ProfilesModel } from '../../models/booking/profiles';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const customersModel = new CustomersModel();
  const reservesModel = new ReservesModel();
  const servicesModel = new ServicesModel();
  const hospitalsModel = new HospitalsModel();
  const periodsModel = new PeriodsModel();
  const usersModel = new UsersModel();
  const serviceTypesModel = new ServiceTypesModel();
  const slotsModel = new SlotsModel();
  const profilesModel = new ProfilesModel();

  fastify.get('/customers/list', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas: any = await customersModel.list(db);

      return reply.status(StatusCodes.CREATED)
        .send({
          status: StatusCodes.CREATED,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementSlot', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any=[]
    let reserves: any
    let reserve: any=[]
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any
    let info :any = []
    let info_ :any = []

    try {

      // data = {
      //slot_date
      //hospital_id
      // }
      slots = await slotsModel.getManagementSlot(db, data);

      for (let v of slots) {
        // slot = v;
        reserves = await reservesModel.getBySlotID(db, v.slot_id);
        for(let x of reserves) {
          // reserve = reserves[0];
          profiles = await profilesModel.getByID(db, x.user_id);
          if (profiles[0]) {
            x.profile = profiles[0];
            // x.profile = profile;
          }
          if(x.customer_id){
            customers = await customersModel.getByID(db, x.customer_id);
            if (customers[0]) {
              x.customer = customers[0];
              // x.customer = customer;
            }          
          }
        }
        if(v.period_id){
          periods = await periodsModel.getByID(db, v.period_id);
          if (periods[0]) {
            v.period = periods[0];
          }        
        }

        if(v.service_type_id){
          service_types = await serviceTypesModel.getByID(db, v.service_type_id);
          if (service_types[0]) {
            v.service_type = service_types[0];
          }
        }

        if(v.hospital_id){
          hospitals = await hospitalsModel.getByID(db, v.hospital_id);
          if (hospitals[0]) {
            v.hospital = hospitals[0];
          }
        }

        // slot = v;
        v.reserve = reserves;
        // slot.period = period;
        // slot.service_type = service_type;
        // slot.hospital = hospital;
        // info.push(slot);

      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: slots
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementReserve', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any
    let reserves: any
    let reserve: any
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any
    let info :any = []

    try {

      // data = {
      //reserve_date
      // }
      reserves = await reservesModel.getManagementReserve(db, data);
      // console.log(reserves);

      for (let v of reserves) {
        // reserve = v;
        profiles = await profilesModel.getByID(db, v.user_id);
        if (profiles[0]) {
          v.profile = profiles[0];
          // v.profile = profile;
        }
        customers = await customersModel.getByID(db, v.customer_id);

        if (customers[0]) {
          v.customer = customers[0];
          // v.customer = customer;
        }

        slots = await slotsModel.getByID(db, v.slot_id);

        for(let x of slots) {
          // slot = x;
          periods = await periodsModel.getByID(db, x.period_id);
          if (periods[0]) {
            x.period = periods[0];
            // x.period = period;
          }
          service_types = await serviceTypesModel.getByID(db, x.service_type_id);
          if (service_types[0]) {
            x.service_type = service_types[0];
            // x.service_type = service_type;
          }
          hospitals = await hospitalsModel.getByID(db, x.hospital_id);
          if (hospitals[0]) {
            x.hospital = hospitals[0];
            // hospital = service_type
          }
          // slot = x;
          x.reserve = [v];
          // slot.period = period;
          // slot.service_type = service_type;
          // slot.hospital = hospital;
          info.push(x);
        }
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementCustomer', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any
    let reserves: any
    let reserve: any
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any
    let info :any = []
    try {

      // data = {
      //searchtext
      // }
      customers = await customersModel.getManagementCustomer(db, data);

      for(let c of customers) {
        // customer = customers[0];

       
        reserves = await reservesModel.getByCustomerID(db, c.customer_id);

        for(let v of reserves) {
          // reserve = v;
          profiles = await profilesModel.getByID(db, v.user_id);
          if (profiles[0]) {
            v.profile = profiles[0];
            // reserve.profile = profile;
          }

          slots = await slotsModel.getByID(db, v.slot_id);
          for(let x of slots) {
            // slot = x;
            periods = await periodsModel.getByID(db, x.period_id);
            if (periods[0]) {
              x.period = periods[0];
              // period = period;
            }
            service_types = await serviceTypesModel.getByID(db, x.service_type_id);
            if (service_types[0]) {
              x.service_type = service_types[0];
              // service_type = service_type;
            }
            hospitals = await hospitalsModel.getByID(db, x.hospital_id);
            if (hospitals[0]) {
              x.hospital = hospitals[0];
              // hospital = service_type
            }
            v.customer = c;
            x.reserve = [v]
            info.push(x);

          }

        }
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/openSlot/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas: any = await slotsModel.updateSlot(db,id);

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/countByHospitalID/:hospital_id/:service_type_id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const hospital_id:number = req.params.hospital_id;
    const service_type_id:number = req.params.service_type_id;

    let data_total: any = [];
    let data_day: any  = [];
    let data_confirm: any  = [];

    let total: any  = 0;
    let appoint_to_day: any = 0;
    let appoint_to_confirm: any = 0;

    try {
      if(service_type_id){
        data_total = await reservesModel.countByHospitalID(db,hospital_id,service_type_id);
      }else{
        data_total = await reservesModel.countByHospitalIDAll(db,hospital_id);
      }
      if(service_type_id){
        data_day = await reservesModel.countByHospitalIDToDay(db,hospital_id,service_type_id);
      }else{
        data_day = await reservesModel.countByHospitalIDToDayAll(db,hospital_id);
      }
      if(service_type_id){
        data_confirm = await reservesModel.countByHospitalIDToConfirm(db,hospital_id,service_type_id);
      }else{
        data_confirm= await reservesModel.countByHospitalIDToConfirmAll(db,hospital_id);
      }


      if(data_total.length > 0){
        total = data_total[0].total;
      }
      if(data_day.length > 0){
        appoint_to_day = data_day[0].appoint_to_day;
      }
      if(data_confirm.length > 0){
        appoint_to_confirm = data_confirm[0].appoint_to_confirm;
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: {data_total : total ,data_day : appoint_to_day,data_confirm : appoint_to_confirm }
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })


  done();
}