import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { QueryModel } from '../../models/booking/query';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importModel = new QueryModel();

  fastify.get('/slot/:dateStart/:dateEnd', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const dateStart:any = req.params.dateStart;
    const dateEnd:any = req.params.dateEnd;

    try {
      let datas :any = await importModel.list(db,dateStart,dateEnd);

      return reply.status(StatusCodes.OK)
        .send({
          status:StatusCodes.OK,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })


  done();
}