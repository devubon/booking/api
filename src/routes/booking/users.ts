import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { UsersModel } from '../../models/booking/users';
import { ProfilesModel } from '../../models/booking/profiles';
import { HospitalsModel } from '../../models/booking/hospitals';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importModel = new UsersModel();
  const profilesModel = new ProfilesModel();
  const hospitalsModel = new HospitalsModel();

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas: any = await importModel.list(db);
      for (let v of datas) {
        delete v.password_hash;
        let res_profile = await profilesModel.getByID(db,v.user_id)
        if(res_profile.length > 0){
          v.profile = res_profile[0];
          let res_hospital = await hospitalsModel.getByID(db,res_profile[0].hospital_id)
          if(res_hospital.length > 0){
            v.hospital = res_hospital[0];
          }
        }
      }
      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/getByID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    try {
      let datas: any = await importModel.getByID(db, id);
      for (let v of datas) {
        delete v.password_hash;
      }
      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/getByRoleID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    try {
      let datas: any = await importModel.getByRoleID(db, id);
      for (let v of datas) {
        delete v.password_hash;
      }
      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
      let datas: any = await importModel.create(db, data);
      for (let v of datas) {
        delete v.password_hash;
      }
      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.put('/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    const data: any = { ...req.body }

    try {
      let datas: any = await importModel.update(db, data, id);
      for (let v of datas) {
        delete v.password_hash;
      }

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.delete('/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id: number = req.params.id;
    try {
      let datas: any = await importModel.delete(db, id);
      if (datas) {
        return reply.status(StatusCodes.OK)
          .send({
            status: StatusCodes.OK,
            ok: true,
            results: `Delete ID : ${id} Successfully`
          });
      } else {
        return reply.status(204)
          .send({
            status: 204,
            ok: false,
            results: `Delete ID : ${id} Not Success`
          });

      }

    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  fastify.get('/getByHospitalID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getByHospitalID(db,id);
      for (let v of datas) {
        delete v.password_hash;
        let res_profile = await profilesModel.getByID(db,v.user_id)
        if(res_profile.length > 0){
          v.profile = res_profile[0];
          let res_hospital = await hospitalsModel.getByID(db,res_profile[0].hospital_id)
          if(res_hospital.length > 0){
            v.hospital = res_hospital[0];
          }
        }
      }
      // for(let v of datas) {
      //   delete v.password_hash;
      // }
      return reply.status(StatusCodes.OK)
        .send({
          status:StatusCodes.OK,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  done();
}