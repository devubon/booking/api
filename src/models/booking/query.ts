import { Knex } from 'knex';

export class QueryModel {

  list(db: Knex,dateStart:any,dateEnd:any) {
    let sql = `select s2.service_name, st.service_type_name ,s.slot_name ,h.hospital_name 
    ,s.slot_date, p.period_name, s.total_slot_per_period ,s.slot_avialable ,s.is_active ,s.service_id ,s.service_type_id ,s.hospital_id
    from slots s 
    inner join services s2 on s.service_id  = s2.service_id 
    inner join service_types st on s.service_type_id  = st.service_type_id 
    inner join hospitals h on s.hospital_id  = h.hospital_id 
    inner join periods p on s.period_id = p.period_id 
    where s.slot_date between ? and ?`
    return db.raw(sql,[dateStart,dateEnd]);
  }  


}