import { Knex } from 'knex';

export class PeriodsModel {

  list(db: Knex) {
    return db('periods')
    .orderBy('start_time', 'asc');
  }  

  info(db: Knex) {
    return db('periods')
    .where('is_active',true)
    .orderBy('start_time', 'asc');
  }  

  getByID(db: Knex, id: number) {
    return db('periods')
    .where('period_id', id)
    .andWhere('is_active', true);
  }  

  getByHospitalID(db: Knex, id: number) {
    return db('periods')
    .where('hospital_id', id)
    .andWhere('is_active', true);
  }  

  getByHospitalIDToManagement(db: Knex, id: number) {
    return db('periods')
    .where('hospital_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('periods')
    .whereLike('period_name', `%${text}%`)
    .andWhere('is_active', true);
  }  

  create(db: Knex, data: any) {
    return db('periods')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('periods')
    .where('period_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('periods')
      .where('period_id', id)
      .delete();
  }

}