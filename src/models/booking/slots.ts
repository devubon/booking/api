import { Knex } from 'knex';

export class SlotsModel {

  list(db: Knex) {
    return db('slots')
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  info(db: Knex) {
    return db('slots')
      .where('is_active', true)
      .andWhere('slot_avialable', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByID(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }


  getByPeriodID(db: Knex, id: number) {
    return db('slots')
      .where('period_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByServiceID(db: Knex, id: number) {
    return db('slots')
      .where('service_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('slots')
      .where('service_type_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByHospitalID(db: Knex, id: number) {
    return db('slots')
      .where('hospital_id', id)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByHospitalIDToManagement(db: Knex, id: number) {
    return db('slots')
      .where('hospital_id', id)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getLookupByHospitalID(db: Knex, id: number) {
    return db('slots')
      .where('hospital_id', id)
      .andWhere('is_active', true)
      .andWhere('slot_avialable', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getSearch(db: Knex, text: string) {
    return db('slots')
      .whereLike('slot_name', `%${text}%`)
      .andWhere('is_active', true)
      .where('slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  create(db: Knex, data: any) {
    return db('slots')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('slots')
      .where('slot_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .delete();
  }

  getManagementSlot(db: Knex, data: any) {
    return db('slots')
      .where('slot_date', data.slot_date)
      .andWhere('hospital_id', data.hospital_id)
      .andWhere('service_id', data.service_id)
      .andWhere('is_active', true);
  }

  getLookupService(db: Knex) {
    return db('slots').select('services.service_id', 'services.service_name', 'service_types.service_type_id', 'service_types.service_type_name','service_types.note','service_types.icon_filename')
      .leftJoin('services', 'slots.service_id', 'services.service_id')
      .leftJoin('service_types', 'slots.service_type_id', 'service_types.service_type_id')
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('slots.is_active', true)
      .andWhere('slots.slot_avialable', true)
      .groupBy('services.service_id', 'services.service_name', 'service_types.service_type_id', 'service_types.service_type_name','service_types.icon_filename');
  }

  getLookupDateSlot(db: Knex, service_type_id: number) {
    return db('slots').select('slots.slot_id', 'slots.slot_date', 'slots.slot_name', 'slots.hospital_id', 'hospitals.hospital_name', 'periods.period_name')
      .leftJoin('hospitals', 'slots.hospital_id', 'hospitals.hospital_id')
      .leftJoin('periods', 'slots.period_id', 'periods.period_id')
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('slots.service_type_id', service_type_id)
      .andWhere('slots.is_active', true)
      .andWhere('slots.slot_avialable', true);
  }

  getLookupHospital(db: Knex) {
    return db('slots').select('hospitals.*')
      .leftJoin('hospitals', 'slots.hospital_id', 'hospitals.hospital_id')
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('slots.is_active', true)
      .andWhere('slots.slot_avialable', true);
  }

  getLookupCustomer(db: Knex, user_id: number) {
    return db('reserves').select('customers.*')
      .leftJoin('customers', 'reserves.customer_id', 'customers.customer_id')
      .where('reserves.user_id', user_id)
  }

  updateSlot(db: Knex, id: number) {
    let data:any = {
      slot_avialable:true
    }
    return db('slots')
      .where('slot_id', id)
      .andWhere('slots.slot_avialable', false)
      .update(data)
      .returning('*');
  }

}