import { Knex } from 'knex';

export class ReservesModel {

  list(db: Knex) {
    return db('reserves')
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))

  }

  getByID(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByCustomerID(db: Knex, id: number) {
    return db('reserves')
      .where('customer_id', id)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getBySlotID(db: Knex, id: number) {
    return db('reserves')
      .where('slot_id', id)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByUserID(db: Knex, id: number) {
    return db('reserves')
      .where('user_id', id)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('reserves')
      .where('service_type_id', id)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }
  getSearch(db: Knex, text: string) {
    return db('reserves')
      .whereLike('reserve_note', `%${text}%`)
      .where('reserve_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  create(db: Knex, data: any) {
    return db('reserves')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .delete();
  }

  getManagementReserve(db: Knex, data: any) {
    let service_id = data.service_id;
    if(service_id){
      return db('reserves').leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
      // .where('reserves.reserve_date', data.reserve_date)
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('slots.hospital_id', data.hospital_id)
      .andWhere('slots.service_id', data.service_id)
    }else{
      return db('reserves').leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
      // .where('reserves.reserve_date', data.reserve_date)
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('slots.hospital_id', data.hospital_id)
      // .andWhere('slots.service_id', data.service_id)
    }


  }

  countByID(db: Knex, id: number) {
    return db('reserves')
      .count('reserve_id as total')
      .where('slot_id', id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
    // .andWhere('is_active', true);
    })
  }

  getSlotPerPeriod(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .select('slots.total_slot_per_period as total');
  }

  setSlotUnavialable(db: Knex, id: number, data: any) {
    return db('slots')
      .where('slot_id', id)
      .update(data)
      .returning('*');
  }

  getPersonReserve(db: Knex, id: any) {
    return db('reserves')
    .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
      .andWhere('reserves.user_id', id)
      .orderBy('slots.slot_date', 'DESC')
  }

  countByHospitalID(db: Knex, hospital_id: number, service_type_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as total')
      .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      })
      .andWhere('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }
  countByHospitalIDAll(db: Knex, hospital_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as total')
      // .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      })
      .andWhere('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  countByHospitalIDToDay(db: Knex, hospital_id: number, service_type_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as appoint_to_day')
      .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      }).andWhere('slots.slot_date', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  countByHospitalIDToDayAll(db: Knex, hospital_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as appoint_to_day')
      // .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      }).andWhere('slots.slot_date', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }


  countByHospitalIDToConfirm(db: Knex, hospital_id: number, service_type_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as appoint_to_confirm')
      .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere('reserves.is_confirm', false)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      }).andWhere('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  countByHospitalIDToConfirmAll(db: Knex, hospital_id: number) {
    return db('reserves')
      .leftJoin('slots', 'reserves.slot_id', 'slots.slot_id')
      .count('reserves.reserve_id as appoint_to_confirm')
      // .where('reserves.service_type_id', service_type_id)
      .andWhere('slots.hospital_id', hospital_id)
      .andWhere('reserves.is_confirm', false)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      }).andWhere('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

  getByUserIDlimit(db: Knex, id: number) {
    return db('reserves')
    .leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
      .where('reserves.customer_id', id).orderBy('slots.slot_date','desc').limit(3)
  }

  getSloteByCustomerID(db: Knex, id: number) {
    return db('reserves').select('reserves.*')
      .leftJoin('slots', 'slots.slot_id', 'reserves.slot_id')
      .where('reserves.customer_id', id)
      .andWhere(function () {
        this.where('reserves.status', '<>', 'cancel')
          .orWhereNull('reserves.status')
      })
      .where('slots.slot_date', '>=', db.raw(`(CURRENT_TIMESTAMP AT TIME ZONE 'UTC-7' ):: date`))
  }

}